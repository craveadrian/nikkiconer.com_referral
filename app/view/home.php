<div id="content">
	<div id="section1">
		<div class="row">
			<div class="left col-6 inbTop">
				<img src="public/images/content/HP-section-1/img1.png" alt="Books" class="books">
				<a href="<?php echo URL ?>contact#paypal" class="btn2">Purchase pre-sell now! </a>
			</div>
			<div class="right col-11 inbTop">
				<div class="s1text">
					<h1>The Quiet Storm</h1>
					<p>In “The Quiet Storm”, Nicole shares the importance of staying wholes during trying times. She shares never-spoken-of events of a little girl who grew in to an ambitious woman seeking nothing but the best for her family. Often misunderstood, she shines light on life through her yes. Her pain brings a different set of lenses to the forefront. Nicole shares the transition of rebirthing herself and her faith. When all hope was lost, she chose to delve deep into her inner strength to regain her life back.</p>
					<a href="<?php echo URL ?>testimonials#content" class="btn">READ MORE</a>
				</div>
			</div>
		</div>
	</div>
	<div id="section2">
		<div class="row">
			<h1>EVENTS</h1>
			<div class="left col-5 inbMid">
				<img src="public/images/content/HP-section-2/img1.jpg" alt="Book">
			</div>
			<div class="right col-7 inbMid">
				<h2>She Believed Volume II</h2>
				<p>14 Stories of Unapologetic Determined and Fearless Women Pursuing Destiny.</p>
				<p>Book Launch & Women’s Empowerment Event</p>
				<p>October 27 2018  |  10 AM - 3 PM</p>
				<p>Double Tree Suites by Hilton <br> 11260 Point East Drive, Rancho Cordova, CA 95742</p>
				<a href="<?php echo URL ?>services#content" class="btn">REGISTER NOW</a>
			</div>
		</div>
	</div>
	<div id="section3">
		<div class="row">
			<p>Be Stronger Than Your Excuses</p>
		</div>
	</div>
	<div id="section4">
		<div class="row">
			<div class="left inbTop">
				<h1>About Nikki</h1>
				<p>Nicole was born and raised in Oakland, California by two hardworking parents who laid a firm foundation instilling in her</p>
				<p>perseverance, love, forgiveness,  & how to overcome <br>life’s hurdles against all odds.</p>
				<p>She’s a wife first and a business coach. She is the owner of Divine Connections Network where she hosts unique events bringing networkers together and privately coaches business professionals to achieve their goals. She hosts events to bring people together to network and share their passion in life. In addition to her networking events twice a year, Nicole hosts two major yearly events one that supports those wanting to wovercome challeges in their marriage and another to teach how to use their pain for their prosperity.</p>
				<a href="<?php echo URL ?>testimonials#content" class="btn">READ MORE</a>
			</div>
			<div class="right inbTop">
				<img src="public/images/content/HP-section-4/img1.jpg" alt="Nikki Coner">
			</div>
		</div>
	</div>
	<div id="contact-section">
		<div class="cntTop">
			<div class="row">
				<h2>CONTACT INFO</h2>
				<div class="info inbTop">
					<img src="public/images/common/email.png" alt="Email">
					<p class="email"><?php $this->info(["email","mailto"]); ?></p>
				</div>
				<div class="info inbTop mid">
					<img src="public/images/common/phone.png" alt="Phone">
					<p class="phone"><?php $this->info(["phone","tel"]); ?></p>
				</div>
				<div class="info inbTop">
					<img src="public/images/common/social.png" alt="Social">
					<p class="socialMedia">
						<a href="<?php $this->info("fb_link"); ?>" target="_blank" class="socialico">f</a>
						<a href="<?php $this->info("tt_link"); ?>" target="_blank" class="socialico">l</a>
						<a href="<?php $this->info("yt_link"); ?>" target="_blank" class="socialico">x</a>
						<a href="<?php $this->info("rss_link"); ?>" target="_blank" class="socialico">r</a>
					</p>
				</div>
			</div>
		</div>
		<div class="cntMid">
			<div class="row">
				<p>Prove to those that want to see you fail that <br> you can also fly without wings.</p>
			</div>
		</div>
		<div class="cntBot">
			<div class="row">
				<div class="qkForm">
					<h1>Get Connected</h1>
					<form action="sendContactForm" method="post"  class="sends-email ctc-form" >
						<input type="text" name="name" placeholder="Name:">
						<input type="text" name="email" placeholder="Email:" class="mid">
						<input type="text" name="phone" placeholder="Phone:">
						<textarea name="message" cols="30" rows="10" placeholder="Message / Questions:"></textarea>
						<div class="g-recaptcha"></div>
						<label>
							<input type="checkbox" name="consent" class="consentBox">I hereby consent to having this website store my submitted information so that they can respond to my inquiry.
						</label><br>
						<?php if( $this->siteInfo['policy_link'] ): ?>
						<label>
							<input type="checkbox" name="termsConditions" class="termsBox"/> I hereby confirm that I have read and understood this website's <a href="<?php $this->info("policy_link"); ?>" target="_blank">Privacy Policy.</a>
						</label>
						<?php endif ?>
						<button type="submit" class="ctcBtn btn" disabled>SUBMIT</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
